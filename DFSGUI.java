/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;
//import finalProject.FileDetails;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.util.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.FlowPane;

/**
 *
 * @author Aparna
 */
public class DFSGUI extends Application implements EventHandler<ActionEvent>, ChangeListener<TreeItem<String>> {

    Stage primaryStg;
    TreeView tv;
    TreeView tv2;
    ListView listView;
    Button upldBtn, selFileBtn, browseBtn, UpldFileBtn, okBtn;
    Scene scene1, scene2, scene3;
    TextField file, destPath;
    String filePath, FileName, dirStructure, fileSz, fileContent, destinationPath;
    Date lastModOn;
    ArrayList<FileDetails> testData = new ArrayList<>();
    ArrayList<FileDetails> selectData = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    public void start(Stage primaryStage) {
        tv = new TreeView<>();
        testData = Client.getInstance().getHomeDir();
        Collections.sort(testData, new DirectoryNameSort());
        tv.setRoot(getNodesForDirectoryFromArrayList("/home", ""));
        tv.getSelectionModel().selectedItemProperty().addListener(this);
        tv2 = new TreeView<>();
        tv2.setRoot(getNodesForDirectoryFromArrayList("/home", ""));
        tv2.getSelectionModel().selectedItemProperty().addListener(this);
        HBox hb = new HBox();
        FlowPane fp = new FlowPane();
        HBox hb1 = new HBox();
        HBox hb2 = new HBox();
        HBox hbTreeView = new HBox();
        primaryStg = primaryStage;
        Label destPathLbl = new Label("Destination Path");
        destPath = new TextField();
        destPath.setPrefWidth(500);
        Label fileNameLbl = new Label("Filename");
        file = new TextField();
        file.setPrefWidth(500);
        browseBtn = new Button("Browse");
        browseBtn.setOnAction(e -> browseBtnButtonClick(e));
        hb.setPadding(new Insets(10, 10, 10, 10));
        hb.setSpacing(700);
        hb1.setPadding(new Insets(10, 10, 10, 10));
        hb1.setSpacing(72);
        fp.setPadding(new Insets(10, 10, 10, 10));
        fp.setVgap(5);
        fp.setHgap(100);
        fp.setPrefWrapLength(800);
        hb2.setPadding(new Insets(10, 10, 10, 10));
        hb2.setSpacing(20);
        hbTreeView.setPadding(new Insets(10, 10, 10, 10));
        upldBtn = new Button("Upload");
        upldBtn.setOnAction(e -> upldBtnButtonClick(e));
        upldBtn.setPrefSize(100, 20);
        selFileBtn = new Button("Select File");
        selFileBtn.setOnAction(e -> selFileBtnButtonClick(e));
        selFileBtn.setPrefSize(100, 20);
        UpldFileBtn = new Button("Upload File");
        UpldFileBtn.setOnAction(e -> UpldFileBtnButtonClick(e));
        UpldFileBtn.setPrefSize(100, 20);
        hb.getChildren().addAll(upldBtn, selFileBtn);
        hb1.getChildren().addAll(fileNameLbl, file, browseBtn);
        hb2.getChildren().addAll(destPathLbl, destPath);
        fp.getChildren().addAll(hb1, hb2);
        hbTreeView.getChildren().addAll(tv2);
        listView = new ListView();
        okBtn = new Button("Ok");
        okBtn.setOnAction(e -> okBtnButtonClick(e));
        BorderPane bp = new BorderPane();
        scene1 = new Scene(bp, 900, 500, Color.WHITE);
        BorderPane bp1 = new BorderPane();
        scene2 = new Scene(bp1, 900, 500, Color.WHITE);
        BorderPane bp2 = new BorderPane();
        scene3 = new Scene(bp2, 900, 500, Color.WHITE);
        bp.setTop(tv);
        bp.setBottom(hb);
        bp1.setTop(fp);
        bp1.setCenter(hbTreeView);
        bp1.setBottom(UpldFileBtn);
        BorderPane.setMargin(UpldFileBtn, new Insets(10, 10, 10, 10));
        bp2.setTop(listView);
        bp2.setBottom(okBtn);
        BorderPane.setMargin(okBtn, new Insets(10, 10, 10, 10));
        primaryStage.setTitle("File Distribution");
        primaryStage.setScene(scene1);
        primaryStage.show();
    }

    /**
     * Upload button in Scene 1
     *
     * @param e
     */
    public void upldBtnButtonClick(ActionEvent e) {
        if (e.getSource() == upldBtn) {
            primaryStg.setScene(scene2);
        } else {
            primaryStg.setScene(scene1);
        }
    }

    /**
     * To fetch file details
     *
     * @param e
     */
    public void browseBtnButtonClick(ActionEvent e) {
        try {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File("E:/Distributed Systems"));
            File selectedFile = fc.showOpenDialog(primaryStg);
            if (selectedFile != null) {
                file.setText(selectedFile.getPath());
                filePath = selectedFile.getPath();
                FileName = selectedFile.getName();
                dirStructure = selectedFile.getParent();
                //length in bytes
                long fileSize = selectedFile.length();
                //file size in string
                fileSz = String.valueOf(fileSize);
                lastModOn = new Date(selectedFile.lastModified());
                destinationPath = destPath.getText();
                /*code to read content of the file*/
                fileContent = "";
                FileReader fileReader = new FileReader(filePath);
                BufferedReader reader = new BufferedReader(fileReader);
                StringBuilder sb = new StringBuilder();
                String line = reader.readLine();
                while (line != null) {
                    sb.append(line).append("\n");
                    line = reader.readLine();
                }
                fileContent = sb.toString();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("File not selected or invalid file.");
                alert.showAndWait();
            }
        } catch (FileNotFoundException e1) {
            System.out.println(e1);
        } catch (IOException e2) {
            System.out.println(e2);
        }
    }

    /**
     * Upload button in Scene 2. From here call to server for storing file
     *
     * @param e
     */
    public void UpldFileBtnButtonClick(ActionEvent e) {
        destinationPath = destPath.getText();
        FileDetails fd = new FileDetails(FileName, destinationPath, "User", "User", lastModOn, fileSz);
        Client.getInstance().createFile(fd);
        primaryStg.setScene(scene1);
        start(primaryStg);
    }

    /**
     * To construct directory structure in tree view Scene 1
     *
     *
     *
     */
    public TreeItem<String> getNodesForDirectoryFromArrayList(String dirName, String openDir) {
        TreeItem<String> root = new TreeItem<>(dirName.substring(dirName.lastIndexOf("/") + 1));
        if (dirName == openDir) {
            root.setExpanded(true);
        }
        for (FileDetails f : testData) {
            if (f.isDir && f.dirName.equals(dirName)) { //Then we call the function recursively
                root.getChildren().add(getNodesForDirectoryFromArrayList(f.dirName + "/" + f.fileName, ""));
            } else if (!f.isDir && f.dirName.equals(dirName)) {
                root.getChildren().add(new TreeItem<>(f.fileName));
            }
        }
        return root;
    }

    public void okBtnButtonClick(ActionEvent e) {
        if (e.getSource() == okBtn) {
            primaryStg.setScene(scene1);
        } else {
            primaryStg.setScene(scene3);
        }
    }

    @SuppressWarnings("unchecked")
    public void selFileBtnButtonClick(ActionEvent e) {
        if (e.getSource() == selFileBtn) {
            primaryStg.setScene(scene3);

            listView.getItems().add(selectData.get(0).fileName);
            listView.getItems().add(selectData.get(0).dirName);
            listView.getItems().add(selectData.get(0).modifiedOn);
            listView.getItems().add(selectData.get(0).fileSize);
        } else {
            primaryStg.setScene(scene1);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue, TreeItem<String> newValue) {

        String scene1DirName, scene1FileName;
        if (primaryStg.getScene() == scene2) {
            // TreeItem<String> root = new TreeItem<>(directory.getName());
            // destPath.setText(newValue.getParent().getValue());
            // root.get().add(getNodesForDirectory(f));

        }
        if (primaryStg.getScene() == scene1) {
            if (newValue.getParent() != null) {
                scene1DirName = "/home/" + newValue.getParent().getValue();
                scene1FileName = "/" + newValue.getValue();
                selectData = Client.getInstance().getDirDetails(scene1DirName + scene1FileName);

            }

        }
    }
}
