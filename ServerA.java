/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author neera
 */
public class ServerA extends UnicastRemoteObject implements ClientServerInterface {

    private static final String SERVER_NAME = "server";
    HashMap<String, FileDetails> hmap = new HashMap<>();

    public ServerA() throws RemoteException {
    }

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.createRegistry(5095);
            registry.rebind(SERVER_NAME, new ServerA());
            System.out.println("Hello ServerA");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/**
 * stored in hashmap using key value Directory + fileName
 * @throws RemoteException 
 */
    @Override
    public void createFile(FileDetails detail1) throws RemoteException {
        hmap.put(detail1.dirName + "/" + detail1.fileName, detail1);

        printContents();
    }

    /**
     *
     * Print contents from Hashmap
     */

    public void printContents() {
        Set set = hmap.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            System.out.print("key is: " + mentry.getKey() + "\n");
            FileDetails detail1 = (FileDetails) mentry.getValue();
            System.out.println("FileName: " + detail1.fileName);
            System.out.println("DirPath: " + detail1.dirName);
            System.out.println("CreatedBy: " + detail1.createdBy);
            System.out.println("Last modified: " + detail1.modifiedOn);
            System.out.println("FileSize: " + detail1.fileSize);
        }
    }

    @Override
    public ArrayList<FileDetails> getDirDet(String dirPath) {
        Set set = hmap.entrySet();
        ArrayList<FileDetails> fileDetList = new ArrayList<>();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry mentry = (Map.Entry) it.next();
            if (((String) mentry.getKey()).contains(dirPath)) {
                fileDetList.add((FileDetails) mentry.getValue());
            }
        }
        return fileDetList;
    }
}
