/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;

/**
 *
 * @author neera
 */
public class Server extends UnicastRemoteObject implements ClientServerInterface {

    private static final String SERVER_NAME = "server";

    static int TOTAL_SERVERS = 3;
    static HashMap<Integer, ServerDetails> serverMap = new HashMap<Integer, ServerDetails>();

    public Server() throws RemoteException {
    }

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.createRegistry(5099);
            registry.rebind(SERVER_NAME, new Server());
            locateServer();
            System.out.println("Hello Server ready");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/**
 * Calls getServerId function to get ASCII value
 **/
    @Override
    public void createFile(FileDetails detail1) throws RemoteException {
        String fileName;
        String dirName;
        String concatName;
        fileName = detail1.fileName;
        dirName = detail1.dirName;
        concatName = dirName + "/" + fileName;
        int serverId = getServerId(concatName);
        try {
            createRmiConnection(serverId).createFile(detail1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/**
 * Function to generate ASCII value process
 * @return 
 */
    private int getServerId(String concatName) {
        String fileASCII;
        fileASCII = concatName;
        int nameLength = concatName.length();
        int asciiSum = 0;

        for (int i = 0; i < nameLength; i++) {
            char character = fileASCII.charAt(i);
            int ascii = (int) character;
            asciiSum += ascii;
        }
        int serverId = asciiSum % TOTAL_SERVERS;

        System.out.println(" serverId " + serverId);
        return serverId;
    }

    private static void locateServer() {
        ServerDetails serverA = new ServerDetails("127.0.0.1", 5095);
        serverMap.put(0, serverA);

        ServerDetails serverB = new ServerDetails("127.0.0.1", 5096);
        serverMap.put(1, serverB);

        ServerDetails serverC = new ServerDetails("127.0.0.1", 5097);
        serverMap.put(2, serverC);
    }

    /**
     * Get details from several servers 
     * 
     */
    @Override
    public ArrayList<FileDetails> getDirDet(String dirPath) {
        ArrayList<FileDetails> dirDetList = new ArrayList<>();
        for (int i = 0; i < TOTAL_SERVERS; i++) {
            try {
                dirDetList.addAll(createRmiConnection(i).getDirDet(dirPath));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dirDetList;
    }
/**
 * Place RMI to call server
 * 
 */
    private ClientServerInterface createRmiConnection(int serverId) {
        ClientServerInterface serverCall = null;
        try {
            ServerDetails serv = serverMap.get(serverId);
            String ServerURL = "rmi://" + serv.ipAddress + ":" + serv.portNumber + "/server";
            serverCall = (ClientServerInterface) Naming.lookup(ServerURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serverCall;
    }
}
