package finalproject;

import java.rmi.Remote;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author neera
 */
import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;

interface ClientServerInterface extends Remote {

    public void createFile(FileDetails detail1) throws RemoteException;
    public ArrayList<FileDetails> getDirDet(String dirPath) throws RemoteException;

}
