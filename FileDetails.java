/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author neera
 */
public class FileDetails implements Serializable{
    /**
     * @param args the command line arguments
     */
    String fileName;
    String dirName;
    String createdBy;
    String modifiedBy;
    String fileSize;
    Date createdOn;
    Date modifiedOn;
    boolean isDir = false;
    public FileDetails(String fileName, String dirName, String createdBy, String modifiedBy, Date modifiedOn, String fileSize) {
        this.fileName = fileName;
        this.dirName = dirName;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.modifiedOn = modifiedOn;
        this.fileSize = fileSize;
    }
    
    public FileDetails(String fileName, String dirName) {
        this.fileName = fileName;
        this.dirName = dirName;
    }
    
    public FileDetails(String fileName, String dirName, Date modifiedOn ,String fileSize)
    {
        this.fileName = fileName;
        this.dirName = dirName;
        this.modifiedOn = modifiedOn;
        this.fileSize = fileSize;
    }
}