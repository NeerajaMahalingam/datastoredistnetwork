/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import static finalproject.Server.TOTAL_SERVERS;
import java.util.*;
import java.rmi.*;
/**
 *
 * @author neera
 */
import java.util.ArrayList;

public class Client {

    private static Client instance = null;
    private static final String SERVER_URL = "rmi://13.59.132.187:5099/server";
    static String rootDir = "/home";
    static String fileName = "ABC.txt";
    static String dirName = rootDir + "dir/src/";
    ArrayList<FileDetails> homeDirDet = new ArrayList<>();
    ArrayList<FileDetails> selectDirDet = new ArrayList<>();

    protected Client() {
    }

    public static Client getInstance() {
        if (instance == null) {
            instance = new Client();
        }
        return instance;
    }
/**
 * Calls CreateFile calls in main server class
 * 
 */
    public void createFile(FileDetails fd) {
        String dirName = fd.dirName;
        String[] dirArray = dirName.split("/");
        String startDir = rootDir;
        try {
            ClientServerInterface serverCall = (ClientServerInterface) Naming.lookup(SERVER_URL);
            for (String individual : dirArray) {
                if (individual.length() > 0) {
                    FileDetails fd1 = new FileDetails(individual, startDir,
                            fd.createdBy, fd.modifiedBy, fd.modifiedOn, fd.fileSize);
                    fd1.isDir = true;
                    serverCall.createFile(fd1);
                    startDir = startDir + "/" + individual;
                }
            }
            fd.dirName = startDir;
            fd.isDir = false;
            serverCall.createFile(fd);
        } catch (Exception e) {
			System.out.println("create file failed");
            e.printStackTrace();
        }
    }

    /**
     * Calls on load of application
     *  
     */
    public ArrayList<FileDetails> getHomeDir() {
        try {
            ClientServerInterface serverCall = (ClientServerInterface) Naming.lookup(SERVER_URL);
            homeDirDet = serverCall.getDirDet(rootDir);
        } catch (Exception e) {
			System.out.println("Get home dir failed");
            e.printStackTrace();
        }
        return homeDirDet;
    }
/**
 * Calls server to get details  
 * 
 *  
 */
    public ArrayList<FileDetails> getDirDetails(String dirPath) {
        try {
            ClientServerInterface serverCall = (ClientServerInterface) Naming.lookup(SERVER_URL);
            selectDirDet = serverCall.getDirDet(dirPath);
        } catch (Exception e) {
			System.out.println("get dir details failed");
            e.printStackTrace();
        }
        return selectDirDet;
    }
}
