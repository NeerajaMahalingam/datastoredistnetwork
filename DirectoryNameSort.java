/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;

import java.util.*;

/**
 *
 * @author neera
 */
public class DirectoryNameSort implements Comparator<FileDetails> {

    public int compare(FileDetails s1, FileDetails s2) {
        return s1.dirName.compareTo(s2.dirName);
    }
}
